import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProjectModule } from './project/project.module';
import { ClientModule } from './client/client.module';
import { TacheModule } from './tache/tache.module';
import { AuthzModule } from './authz/authz.module';
import { DocumentsModule } from './documents/documents.module';
import { ReunionModule } from './reunion/reunion.module';
import { MessageModule } from './message/message.module';
import { FileModule } from './file/file.module';


@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3307,
      username: 'user',
      password: 'user',
      database: 'mydb',
      entities: ["dist/**/*.entity{.ts,.js}"],
      synchronize: true,
    }),
    ProjectModule,
    ClientModule,
    TacheModule,
    AuthzModule,
    DocumentsModule,
    ReunionModule,
    MessageModule,
    FileModule,
    ],
     

})
export class AppModule {}
