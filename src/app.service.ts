import { Injectable } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';

@Injectable()
@ApiTags('Health')
export class AppService {
  getHello(): string {
    return '';
  }
}
