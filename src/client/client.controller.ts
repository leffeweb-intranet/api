import { Body, Controller, Get, HttpStatus, Param, Post, Response, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBody, ApiTags } from '@nestjs/swagger';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Client, ClientDTO } from './client.entity';
import { ClientService } from './client.service';
import { Permissions } from '../permissions.decorator'
import { PermissionsGuard } from '../permissions.guard';

@Controller('client')
@ApiTags('Client')
export class ClientController {
    constructor(private readonly clientService: ClientService) { }
    @Get()
    async get() {
        const clients = this.clientService.get()
        return clients;
    }
    @Get('/admin/client/:id')
    async getById(
        @Param("id") id: string,
        @Response() response
    ) {
        this.clientService.getById(id).then(
            result => {
                return response.status(HttpStatus.OK).send(result);
            }, error => {
                console.log(error);
                return response.status(HttpStatus.BAD_REQUEST).send(error);
            })
    }


    @UseGuards(AuthGuard('jwt'), PermissionsGuard)
    @Get('/:auth0')
    @Permissions('read:project')
    async getByAuth0(
        @Param('auth0') id_auth0: string,
        @Response() response
    ) {
        console.log("authoo",id_auth0);
        
        return this.clientService.getByIdAuth0(id_auth0).then(
            result => {
                return response.status(HttpStatus.OK).send(result);
            }, error => {
                console.log(error);
                return response.status(HttpStatus.BAD_REQUEST).send(error);
            })
    }

    @UseGuards(AuthGuard('jwt'))
    @Post()
    @ApiBody({ type: ClientDTO })
    @Permissions('read:project')
    async create(
        @Body() client: Client,
        @Response() response
    ) {
        ////console.log(client);
        return this.clientService.addOne(client).then(
            result => {
                return response.status(HttpStatus.OK).send(result);
            }, error => {
                console.log(error);
                return response.status(HttpStatus.BAD_REQUEST).send(error);
            })
    }



}
