import { ApiProperty } from "@nestjs/swagger";
import { Project } from "src/project/project.entity";
import { Message } from "src/message/message.entity";
import { File } from "src/file/file.entity";

import { Column, Entity, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";


@Entity()
export class Adresse{

    @PrimaryGeneratedColumn()
    id: number;
    
    @Column()
    @ApiProperty({example:"France"})
    Pays:string;

    @Column()
    @ApiProperty({example:"13001"})
    CodePostal:number;

    @Column()
    @ApiProperty({example:"49"})
    NumeroDeRue:number;

    @Column()
    @ApiProperty({example:"Boulevard de jesaispas"})
    Rue:string;
}

@Entity()
export class Client{

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    idAuth  ?: string;
    
    @Column({default: false})
    admin  ?: boolean;
 
    @Column()
    @ApiProperty({example:"Bernard"})
    firstName:string;

    @Column()
    @ApiProperty({example:"Myde"})
    lastName:string;

    @Column()
    @ApiProperty({example:"066666666"})
    Tel?:number;


    @OneToOne(() => Adresse,{
        cascade: true,
    })
    @JoinColumn()
    adresse?:Adresse;

    @OneToMany(() => Project, project => project.client)
    project?: Project[];

    @OneToMany(() => Message, message => message.client)
    message?: Message[];
    
    @OneToMany(() => File, file => file.client)
    file?: File[];
    
    

}


export class AdresseDTO{

        @ApiProperty({example:"France"})
        Pays:string;
    

        @ApiProperty({example:"13001"})
        CodePostal:number;
    

        @ApiProperty({example:"49"})
        NumeroDeRue:number;
    

        @ApiProperty({example:"Boulevard de jesaispas"})
        Rue:string;
}


export class ClientDTO{
    
    @ApiProperty({example:"2"})
    id?:string;

    @ApiProperty({example:"Bernard"})
    firstName:string;

    @ApiProperty({example:"Myde"})
    lastName:string;

    @ApiProperty({example:"066666666"})
    Tel:number;


    @ApiProperty({example:"066666666"})
    Admin?:boolean;

    adresse:AdresseDTO;


}