import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Client } from './client.entity';

@Injectable()
export class ClientService {
  constructor(@InjectRepository(Client) private clientsRepository: Repository<Client>) { }


  async addOne(client: Client): Promise<Client> {
    return this.clientsRepository.save(client);
  }

  async get() {
    const clientSaved = await this.clientsRepository.find();
    return clientSaved
  }

  async getById(id: string) {
    console.log("id", id);

    let e = this.clientsRepository.findOne({ idAuth: id });
    console.log(e);
    return e


  }

  async getByIdAuth0(id_auth0: string) {
    return this.clientsRepository.findOne({ idAuth: id_auth0 });
  }
}
