import { Controller, Get, HttpStatus, Param, Post, Res, UploadedFile, UseInterceptors } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';

@Controller('documents')
export class DocumentsController {


    @Get('uploads')
    async try() {
        return "ok"
    }

    @Post('uploads')
    @UseInterceptors(
        FileInterceptor('photo', {
            dest: './uplodas/photo'
        })
    )
    uploadSingle(@UploadedFile() file) {
        //console.log(file);
        return file.path;
    }

    @Get(':imagename')
    getImage(@Param('imagename') image, @Res() res) {
        const response = res.sendFile(image, { root: './uploads' });
        return {
            status: HttpStatus.OK,
            data: response,
        };
    }
}

