import { Body, Controller, Get, Param, Post, Res, UploadedFile, UploadedFiles, UseInterceptors } from '@nestjs/common';
import { FileInterceptor, FilesInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import path, { extname } from 'path';
import { FileService } from './file.service';
import { FileDTO } from './fileDTO'
export const editFileName = (req, file, callback) => {

  const name = encodeURIComponent(file.originalname.split('.')[0]);
  const fileExtName = extname(file.originalname);
  const randomName = Array(4)
    .fill(null)
    .map(() => Math.round(Math.random() * 16).toString(16))
    .join('');
  callback(null, `${name}-${randomName}${fileExtName}`);
};

@Controller('file')
export class FileController {
  constructor(private readonly fileService: FileService) { }


  @Post("upload")
  @UseInterceptors(FileInterceptor("file",
    {
      storage: diskStorage({
        destination: './uploads',
        filename: editFileName,
      }),
    }
  ))
  async uploadSingle(
    @UploadedFile() file: Express.Multer.File,
    @Body() infos: FileDTO) {
    console.log(file);

    const obj: FileDTO = JSON.parse(JSON.stringify(infos));

    await this.fileService.upload(obj, file);

    return ({ success: true })
  }

  // @Get('/:projectId')
  // async getByProjectId(
  //     @Param('projectId') projectId: string
  // ) {
  //     console.log("projectId",projectId);

  //     const res = await this.fileService.getByProject(projectId);
  //     console.log(res);

  //     return res
  // }

  @Get('/:projectId/:type')
  async getByProjectIdAndtype(
    @Param('projectId') projectId: string,
    @Param('type') type: string


  ) {
    console.log("projectId", projectId);
    console.log("type", type);

    const res = await this.fileService.getByProject(projectId, type);
    // console.log(res);

    return res
  }


  @Get('up/uploads/:imgpath')
  seeUploadedFile(@Param('imgpath') image, @Res() res) {
    console.log('imgpath');
    return res.sendFile(encodeURIComponent(image), { root: 'uploads' });
    // return {ok:"ok"}
  }
}