import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { File } from "./file.entity";

@Injectable()
export class FileService {
    constructor(@InjectRepository(File) private fileRepository: Repository<File>) { }


    async upload(infos, file) {

        infos.name = file.originalname;
        infos.path = file.path;
        this.fileRepository.save(infos)
    }
    async getByProject(projectId, type) {
        return this.fileRepository.find({
            where: { type: type, project: { id: projectId } },
            // project: {id:projectId}
        });
    }
}