import { Client } from "src/client/client.entity";
import { Project } from "src/project/project.entity";


export interface FileDTO {
    type?: string;
    client?: Client;
    project?: Project;
    path?: string;
    name?: string;

}