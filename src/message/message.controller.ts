import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { MessageService } from './message.service';

@Controller('message')
export class MessageController {
    constructor(private readonly messageService: MessageService) { }




    @Get('/:projectId')
    async getByProjectId(
        @Param('projectId') projectId: string
    ) {
        console.log('projectId =>', projectId);
        const messages = await this.messageService.getByProjectId(projectId);
        return messages;
    }
    @Post()
    async create(
        @Body() message
    ) {
        console.log('message', message);

        console.log("message", message);
        const messageSaved = await this.messageService.create(message);
        return messageSaved;

    }
}
