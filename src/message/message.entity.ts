import { Client } from "src/client/client.entity";
import { Project } from "src/project/project.entity";
import { Status } from "src/tache/tache.entity";
import { Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@Entity()
export class Message {
    @PrimaryGeneratedColumn()
    id: number;

    @Column('varchar', { length: 500 })
    content: string;

    @CreateDateColumn()
    createdAt: string;

    @UpdateDateColumn({ type: "timestamp" })
    updatedAt: number;

    @ManyToOne(() => Project, project => project.reunion)
    project: Project;

    @ManyToOne(() => Client, client => client.message)
    client: Client;

}
