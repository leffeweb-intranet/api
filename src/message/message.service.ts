import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Message } from './message.entity';

@Injectable()
export class MessageService {
  constructor(
    @InjectRepository(Message)
    private messageRepository: Repository<Message>,
  ) { }



  async getByProjectId(projectId) {
    const messages = await this.messageRepository.find({ where: { project: projectId }, relations: ['client'] });
    console.log(`message trouvé pour le projet ${projectId} `, messages);

    return messages
  }

  async create(message) {
    const messageSaved = await this.messageRepository.save(message);
    console.log(`message créer  :`, message);

    return messageSaved
  }

}
