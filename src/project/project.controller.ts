import { Body, Controller, Get, HttpStatus, Param, Post, Response, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBody, ApiParam, ApiProperty, ApiQuery, ApiTags } from '@nestjs/swagger';
import { type } from 'os';
import { Project } from './project.entity';
import { ProjectDTO } from './project.model';
import { ProjectService } from './project.service';
import { Permissions } from '../permissions.decorator'
import { PermissionsGuard } from '../permissions.guard';
import { FileInterceptor } from '@nestjs/platform-express';
import { editFileName } from 'src/utils/file-upload';
import { diskStorage } from 'multer';
import { ClientService } from 'src/client/client.service';
import { Client, ClientDTO } from 'src/client/client.entity';
import { response } from 'express';

@Controller('project')
@ApiTags('project')
export class ProjectController {
    constructor(
        private readonly projectService: ProjectService,
        private readonly clientService: ClientService
    ) { }

    @UseGuards(AuthGuard('jwt'), PermissionsGuard)
    @Get()
    @Permissions('read:project')
    async get() {

        return this.projectService.get().then(
            result => {
                return response.status(HttpStatus.OK).send(result);
            }, error => {
                console.log(error);
                return response.status(HttpStatus.BAD_REQUEST).send(error);
            });

    }



    @UseGuards(AuthGuard('jwt'), PermissionsGuard)
    @Get('/:id')
    @Permissions('read:project')
    async getById(
        @Param() id: number,
        @Response() response,
    ) {

        return this.projectService.getByID(id).then(
            result => {
                return response.status(HttpStatus.OK).send(result);
            }, error => {
                console.log(error);
                return response.status(HttpStatus.BAD_REQUEST).send(error);
            })

    }

    @UseGuards(AuthGuard('jwt'), PermissionsGuard)
    @Get('client/:id')
    @Permissions('read:project')
    async getByAuth(
        @Param('id') id: string
    ) {
        const client: any = await this.clientService.getByIdAuth0(id);
        console.log("client", client);
        if (client.error) return [];
        const projects = await this.projectService.getByClientID(client);
        return projects;
    }
    @UseGuards(AuthGuard('jwt'), PermissionsGuard)
    @Get('admin/client/:id')
    @Permissions('read:project')
    async getByIdUser(
        @Param('id') id: string
    ) {
        const client: any = await this.clientService.getById(id);

        const projects = await this.projectService.getByClientID(client);
        return projects;
    }


    @Post('/i/:auth0')
    @ApiBody({ type: ProjectDTO })
    @Permissions('read:project')
    @UseInterceptors(
        FileInterceptor("file", {
            storage: diskStorage({
                destination: './uploads',
                filename: editFileName,
            }),

        })
    )

    // @Permissions('read:project')
    @Post('/create')
    @UseInterceptors(FileInterceptor("image", { dest: "./uploads" }))
    uploadSingle(@UploadedFile() file) {
        //  console.log(project);
        console.log(file);

        // const projectSaved=this.projectService.addOne(project); 
        // return projectSaved;

    }


    @Post('/:auth0')
    @ApiBody({ type: ProjectDTO })
    @Permissions('read:project')
    async create(
        @Body() project
    ) {
        const projectSaved = await this.projectService.addOne(project);
        return projectSaved;
    }
}