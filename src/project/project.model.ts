import { ApiProperty } from "@nestjs/swagger";

export class ProjectDTO{
  
    @ApiProperty({example:"LaFromagerie"})
    name: string;

    @ApiProperty({example:"Ceci est un project"})
    description ?: string;

    @ApiProperty({example:"http://dezedokzes"})
    image ?: string;

    @ApiProperty({example:"http://dezedokzes"})
    url ?: string;

    @ApiProperty({example:"0"})
    client:number;
    
  }