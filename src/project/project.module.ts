import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProjectController } from './project.controller';
import { ProjectService } from './project.service';
import { Project } from './project.entity';
import { Client } from 'src/client/client.entity';
import { ClientModule } from 'src/client/client.module';
import { ClientService } from 'src/client/client.service';

@Module({
  imports: [
    ClientModule,
    TypeOrmModule.forFeature([
      Project
        ])
  ],
  controllers: [ProjectController],
  providers: [ProjectService],

})
export class ProjectModule {}