import { Injectable } from '@nestjs/common';
import { Project } from './project.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Client } from 'src/client/client.entity';

@Injectable()
export class ProjectService {
  constructor(
    @InjectRepository(Project)
    private projectsRepository: Repository<Project>
  ) { }


  async addOne(project: Project): Promise<Project> {
    return this.projectsRepository.save(project);
  }

  async getByID(id: number) {
    return this.projectsRepository.findOne(id);
  }

  async get() {
    return this.projectsRepository.find();
  }

  async getByClientID(client) {

    return this.projectsRepository.find({ client: client });

  }

  async getByAuth(auth0: any) {


    return this.projectsRepository.find({ client: { idAuth: auth0.auth0 } });
  }




}
