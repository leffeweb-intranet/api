import { Body, Controller, Get, HttpStatus, Param, Post, Response } from '@nestjs/common';
import { ApiBody } from '@nestjs/swagger';
import { response } from 'express';
import { Reunion } from './reunion.entity';
import { ReunionService } from './reunion.service';

@Controller('reunion')
export class ReunionController {
    constructor(
        private readonly reunionService: ReunionService,        ) {}

    @Get('/:projectId')
    async getByProjectId(
        @Param('projectId') projectId:string,
        @Response() response

    )
    {
         return this.reunionService.getByProjectId(projectId).then(
            result => {
                return response.status(HttpStatus.OK).send(result);
            }, error => {
                console.log(error);
                return response.status(HttpStatus.BAD_REQUEST).send(error);
            })
        
    }

    @Post()
    // @ApiBody({ type: ProjectDTO })
    async create(
        @Body() reunion,
        @Response() response
    )
    {
         console.log("reunion",reunion);

        return this.reunionService.create(reunion).then(
            result => {
                return response.status(HttpStatus.OK).send(result);
            }, error => {
                console.log(error);
                return response.status(HttpStatus.BAD_REQUEST).send(error);
            })
        

    }
    
}
