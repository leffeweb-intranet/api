import { Project } from "src/project/project.entity";
import { Status } from "src/tache/tache.entity";
import { Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@Entity()
export class Reunion{
    @PrimaryGeneratedColumn()
    id:number;

    @Column()
    name:string;

    @Column()
    duration:string;


    @Column({ type: 'date' })
    date:Date;


    @Column('time')
    hours:Date;

    @Column({default:"https:google.meet.com"})
    url:string;

    @Column({default:"toDo"})
    status:Status;

    @CreateDateColumn()
    createdAt: string;
    
    @UpdateDateColumn({ type: "timestamp" })
    updatedAt: number;

    @ManyToOne(()=> Project, project => project.reunion)
    project:Project;

}
