import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ReunionController } from './reunion.controller';
import { Reunion } from './reunion.entity';
import { ReunionService } from './reunion.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Reunion
        ])
  ],
  controllers: [ReunionController],
  providers: [ReunionService]
})
export class ReunionModule {}
