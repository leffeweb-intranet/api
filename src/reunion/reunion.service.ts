import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Reunion } from './reunion.entity';

@Injectable()
export class ReunionService {

  constructor(
    @InjectRepository(Reunion)
    private reunionsRepository: Repository<Reunion>
  ) { }

  async getByProjectId(projectId) {
    return this.reunionsRepository.find({ where: { project: projectId } });
  }

  async create(reunion) {
    const reunionSaved = await this.reunionsRepository.save(reunion);
    return reunionSaved
  }
}
