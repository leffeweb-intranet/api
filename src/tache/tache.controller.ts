import { Body, Get, Param, Post, Put } from '@nestjs/common/decorators/http';
import { Controller, HttpStatus, Response } from '@nestjs/common';
import { Tache, TacheDTO } from './tache.entity';
import { TacheService } from './tache.service';
import { ApiBody, ApiTags } from '@nestjs/swagger';

@Controller('tache')
@ApiTags('Tags')
export class TacheController {
    constructor(private readonly tacheService: TacheService) { }


    @Get()
    async get(
        @Response() response
    ) {
        return this.tacheService.get().then(
            result => {
                return response.status(HttpStatus.OK).send(result);
            }, error => {
                console.log(error);
                return response.status(HttpStatus.BAD_REQUEST).send(error);
            })
    }

    @Get('/project/:projectId')
    async getByProject(
        @Param("projectId") projectId: string,
        @Response() response
    ) {
        return this.tacheService.getByProjectId(projectId).then(
            result => {
                return response.status(HttpStatus.OK).send(result);
            }, error => {
                console.log(error);
                return response.status(HttpStatus.BAD_REQUEST).send(error);
            })
    }

    @Post()
    @ApiBody({ type: TacheDTO })
    async create(
        @Body() tache: Tache,
        @Response() response
    ) {
        return this.tacheService.addOne(tache).then(
            result => {
                return response.status(HttpStatus.OK).send(result);
            }, error => {
                console.log(error);
                return response.status(HttpStatus.BAD_REQUEST).send(error);
            })
    }

}



