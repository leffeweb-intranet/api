
import { ApiProperty } from "@nestjs/swagger";
import { Project } from "src/project/project.entity";
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";


@Entity()
export class Tache{
    @PrimaryGeneratedColumn()
    id:number;

    @Column()
    
    name:string;

    @Column()
    description:string;

    @Column({default:"toDo"})
    status:Status;

    @ManyToOne(()=> Project, project => project.tache)
    project:Project;

}

export class TacheDTO{

    @ApiProperty({example:"Design"})
    name:string;

    @ApiProperty({example:"Make a site beautifull"})
    description:string;

    status:Status;

    @ApiProperty({example:"1"})
    project:number;
}

export enum Status {
    progress="inProgress",
    DoIt="doIt",
    Done="done"
  }