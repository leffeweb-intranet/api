import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TacheController } from './tache.controller';
import { Tache } from './tache.entity';
import { TacheService } from './tache.service';

@Module({  
  imports: [TypeOrmModule.forFeature([Tache])],

  controllers: [TacheController],
  providers: [TacheService]
})
export class TacheModule {}
