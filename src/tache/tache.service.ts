import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Tache } from './tache.entity';

@Injectable()
export class TacheService {

  constructor(
    @InjectRepository(Tache)
    private tachesRepository: Repository<Tache>,
  ) { }


  async addOne(tache: Tache): Promise<Tache> {
    return this.tachesRepository.save(tache);
  }

  async getByProjectId(projectId) {

    const toDo = await this.tachesRepository.find({ where: { project: projectId, status: "toDo" } });
    const inProgress = await this.tachesRepository.find({ where: { project: projectId, status: "inProgress" } });
    const done = await this.tachesRepository.find({ where: { project: projectId, status: "done" } });

    let taches: TacheSplit = {
      toDo: toDo,
      inProgress: inProgress,
      done: done
    };

    console.log("taches", taches);

    return taches;
  }


  async get() {
    const taches = await this.tachesRepository.find();
    return taches;
  }



}

export interface TacheSplit {
  toDo: Tache[];
  inProgress: Tache[];
  done: Tache[];
}